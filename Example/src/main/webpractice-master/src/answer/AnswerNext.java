package answer;


import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class Next
 */
@WebServlet("/answer_Next")
public class AnswerNext extends HttpServlet {
	private static final long serialVersionUID = 1L;
	public static final String NEXT = "WEB-INF/jsp/answer/next.jsp";
	public static final String RESULT = "WEB-INF/jsp/answer/result.jsp";

    /**
     * @see HttpServlet#HttpServlet()
     */
    public AnswerNext() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// マルチバイト文字列の文字化け対応
		request.setCharacterEncoding("UTF-8");
		// URLパラメータから値を取得
		String requestValue = request.getParameter("requestValue");
		// リクエスト領域に値をセット
		request.setAttribute("requestValue", requestValue);
		// next.jspに画面遷移
		RequestDispatcher dispatcher = request.getRequestDispatcher(NEXT);
		dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// マルチバイト文字列の文字化け対応
		request.setCharacterEncoding("UTF-8");
		// next.jspで入力した情報を取得
		String requestValue = request.getParameter("requestValue");
		// リクエスト領域に値をセット
		request.setAttribute("requestValue", requestValue);
		// result.jspに画面遷移
		RequestDispatcher dispatcher = request.getRequestDispatcher(RESULT);
		dispatcher.forward(request, response);
	}

}
