package answer;


import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class Next
 */
@WebServlet("/answer_Result")
public class AnswerResult extends HttpServlet {
	private static final long serialVersionUID = 1L;
	public static final String INDEX = "answer_Index";

    /**
     * @see HttpServlet#HttpServlet()
     */
    public AnswerResult() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// index.jspにリダイレクト
		response.sendRedirect(INDEX);
	}
}
