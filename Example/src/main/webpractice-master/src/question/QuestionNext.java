package question;


import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class Next
 */
@WebServlet("/question_Next")
public class QuestionNext extends HttpServlet {
	private static final long serialVersionUID = 1L;
	public static final String NEXT = "WEB-INF/jsp/question/next.jsp";
	public static final String RESULT = "WEB-INF/jsp/question/result.jsp";

    /**
     * @see HttpServlet#HttpServlet()
     */
    public QuestionNext() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// マルチバイト文字列の文字化け対応
		request.setCharacterEncoding("UTF-8");
		// TODO URLパラメータから値を取得
		String requestValue = request.getParameter("requestValue");
		// TODO リクエスト領域から取得したrequestValueの値を再度リクエスト領域にセット
		request.setAttribute("requestValue", requestValue);
		// TODO next.jspに遷移（フィールドの定数を使用すること）
		RequestDispatcher dispatcher = 
				request.getRequestDispatcher(NEXT);
				dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// マルチバイト文字列の文字化け対応
		request.setCharacterEncoding("UTF-8");
		// TODO リクエスト領域に「next.jspのリクエストの値です～」で入力した値を取得
		String requestValue = request.getParameter("requestValue");
		// TODO リクエスト領域に値をセット
		request.setAttribute("requestValue", requestValue);
		// TODO result.jspに遷移（フィールドの定数を使用すること）
		RequestDispatcher dispatcher = 
				request.getRequestDispatcher(RESULT);
				dispatcher.forward(request, response);

	}

}
