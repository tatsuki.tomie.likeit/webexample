package question;


import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Servlet implementation class Index
 */
@WebServlet("/question_Index")
public class QuestionIndex extends HttpServlet {
	private static final long serialVersionUID = 1L;
	public static final String INDEX = "WEB-INF/jsp/question/index.jsp";
	public static final String MENU = "WEB-INF/jsp/question/menu.jsp";

    /**
     * @see HttpServlet#HttpServlet()
     */
    public QuestionIndex() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO index.jspに遷移（フィールドの定数を使用すること）
		RequestDispatcher dispatcher = 
		request.getRequestDispatcher(INDEX);
		dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// マルチバイト文字列の文字化け対応
		request.setCharacterEncoding("UTF-8");
		// セッションインスタンス取得
		HttpSession session = request.getSession();
		// TODO index.jspで入力した情報を取得
		String requestValue = request.getParameter("requestValue");
		String sessionValue = request.getParameter("sessionValue");
		// TODO リクエスト領域に「index.jspのリクエストに入れる値を～」で入力した値をセット
		request.setAttribute("requestValue", requestValue);
		// TODO セッション領域に「index.jspのセッションに入れる値を～」で入力した値をセット
		session.setAttribute("sessionValue",sessionValue);
		// TODO menu.jspに遷移（フィールドの定数を使用すること）
		RequestDispatcher dispatcher = 
				request.getRequestDispatcher(MENU);
				dispatcher.forward(request, response);
	}
}
