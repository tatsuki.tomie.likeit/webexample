package question;


import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class Next
 */
@WebServlet("/question_Result")
public class QuestionResult extends HttpServlet {
	private static final long serialVersionUID = 1L;
	public static final String INDEX = "question_Index";

    /**
     * @see HttpServlet#HttpServlet()
     */
    public QuestionResult() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO index.jspにリダイレクト（フィールドの定数を使用すること）
		response.sendRedirect(INDEX);
	}
}
