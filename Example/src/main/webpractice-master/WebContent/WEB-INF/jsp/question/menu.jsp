<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>menu</title>
</head>
<body>
	<table>
		<tr>
			<td><p>メニューです</p></td>
		</tr>
		<tr>
			<td><p>リクエストの値です：</p></td>
			<td><p>${requestValue }</p></td>
		</tr>
		<tr>
			<td><p>セッションの値です：</p></td>
			<td><p>${sessionValue }</p></td>
		</tr>
		<tr>
			<!-- Next.javaのdoGetメソッドを実行 -->
			<!-- TODO QuestionNext.javaのdoGetメソッド-->
			<td><a href="">次の画面へ</a></td>
		</tr>
	</table>
</body>
</html>