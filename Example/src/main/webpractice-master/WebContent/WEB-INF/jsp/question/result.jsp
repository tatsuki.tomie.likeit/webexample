<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>最後の画面</title>
</head>
<body>
	<table>
		<tr>
			<td><p>リザルトです</p></td>
		</tr>
		<tr>
			<td><p>${requestValue }</p></td>
		</tr>
		<tr>
			<td><p>${sessionValue }</p></td>
		</tr>
		<tr>
			<!-- TODO QuestionResult.javaのdoGetメソッドを実行 -->
			<td><a href="">インデックスへ戻る</a></td>
		</tr>
	</table>
</body>
</html>