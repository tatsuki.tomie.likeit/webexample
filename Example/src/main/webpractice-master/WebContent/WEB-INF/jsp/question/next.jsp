<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>次の画面</title>
</head>
<body>
	<!-- QuestionNext.javaのdoPostメソッドを実行 -->
	<!-- TODO 「action属性」「method属性」に正しい値を入れてください -->
	<form action="/QuestionNext.java" method="post">
		<table>
			<tr>
				<td><p>ネクストです</p></td>
			</tr>
			<tr>
				<td><p>リクエストの値です（書き換えてください）：</p></td>
				<td><p><input type="text" name="requestValue" value="${requestValue }"></p></td>
			</tr>
			<tr>
				<td><p>セッションの値です：</p></td>
				<td><p>${sessionValue }</p></td>
			</tr>
			<tr>
				<td><input type="submit" value="最後の画面へ"></td>
			</tr>
		</table>
	</form>
</body>
</html>