<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>index</title>
</head>
<body>
	<!-- QuestionIndex.javaのdoPostメソッドを実行 -->
	<!-- TODO 「action属性」「method属性」に正しい値を入れてください -->
	<form action="/QuestionIndex.java" method="post">
		<table>
			<tr>
				<td><p>インデックスです</td>
			</tr>
			<tr>
				<td><p>リクエストに入れる値を入力してください</p></td>
				<td><p>：</p></td>
				<td><input type="text" name="requestValue"></td>
			</tr>
			<tr>
				<td><p>セッションに入れる値を入力してください</p></td>
				<td><p>：</p></td>
				<td><input type="text" name="sessionValue"></td>
			</tr>
			<tr>
				<td><input type="submit" value="メニューに行きます"></td>
			</tr>
		</table>
	</form>
</body>
</html>