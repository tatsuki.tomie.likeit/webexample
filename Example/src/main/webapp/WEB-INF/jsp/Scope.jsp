<%@page import="model.Human" %>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>リクエストスコープ取得サンプル</title>
</head>
<body>
<%
Human human = (Human) request.getAttribute("human");
%>
<p><%=human.getName() %></p>
<p><%=human.getAge() %></p>
<p><%=human.getBlood()%></p>
</body>
</html>