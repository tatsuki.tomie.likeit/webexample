package dao;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import model.Employee;

public class EmployeeDao {
	public List<Employee>findAll(){
		Connection conn = null;
		List<Employee>empList =new ArrayList<Employee>();
		
	try {
		
	conn = DBManager.getConnection();
	
	String sql = "SELECT id,name,age FROM employee";
	
	Statement stmt= conn.createStatement();
	ResultSet rs = stmt.executeQuery(sql);
	
	while(rs.next()) {
		String id= rs.getString("id");
		String name = rs.getString("name");
		int age = rs.getInt("age");
		Employee employee = new Employee(id,name,age);
		empList.add(employee);
	}
	} catch (SQLException e) {
        e.printStackTrace();
        return null;
    } finally {
       
    	if (conn != null) {
            try {
                conn.close();
            } catch (SQLException e) {
                e.printStackTrace();
                return null;
            }
        }
    }
    return empList;
}

	}


