package model;

import java.io.Serializable;

public class Human implements Serializable {
	private String name;
	private int age;
	private String blood;

	public Human() {

	}

	public Human(String name, int age, String blood) {
		this.name = name;
		this.age = age;
		this.blood = blood;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public String getBlood() {
		return blood;
	}

	public void setBlood(String blood) {
		this.blood = blood;
	}

}
